package net.streletsky.ngptoolkit;

import de.robv.android.xposed.XSharedPreferences;

public class XSettings {
    final XSharedPreferences settings;

    public XSettings() {
        settings = new XSharedPreferences(BuildConfig.APPLICATION_ID, Settings.CONFIG_ID);
    }

    public boolean getMtpFullAccess() {
        return settings.getBoolean(Settings.MTP_FULL_ACCESS, false);
    }

    public boolean getHomeButtonLongPress() {
        return settings.getBoolean(Settings.HOME_BUTTON_LONG_PRESS, false);
    }

    public boolean getUsbWakeUp() {
        return settings.getBoolean(Settings.USB_WAKEUP, false);
    }

    public boolean getIsStatusBarEnabled() {
        return settings.getBoolean(Settings.ENABLE_STATUS_BAR, false);
    }

    public boolean getRemoveStatusBar() {
        return settings.getBoolean(Settings.REMOVE_STATUS_BAR, false);
    }

    public boolean getIsSleepScreenEnabled() {
        return settings.getBoolean(Settings.IS_SLEEP_SCREEN_ENABLED, false);
    }

    public boolean getIsDefaultLauncherDisabled() {
        return settings.getBoolean(Settings.DISABLE_DEFAULT_LAUNCHER, false);
    }

    public boolean getRandomizeSleepScreenOrder() {
        return settings.getBoolean(Settings.RANDOMIZE_SLEEP_SCREENS, false);
    }

    public boolean getHideStatusBarProfile() {
        return settings.getBoolean(Settings.HIDE_STATUSBAR_PROFILE, false);
    }

    public boolean getHideStatusBarSettings() {
        return settings.getBoolean(Settings.HIDE_STATUSBAR_SETTINGS, false);
    }

    public boolean getReplaceStatusBarSettingsAction() {
        return settings.getBoolean(Settings.REPLACE_STATUSBAR_SETTINGS_ACTION, false);
    }

    public boolean getIsFastRefreshEnabled() {
        return settings.getBoolean(Settings.IS_FAST_REFRESH_ENABLED, false);
    }

    public boolean getIsLowBatteryWarningDisabled() {
        return settings.getBoolean(Settings.DISABLE_LOW_BATTERY_WARNING, false);
    }

    public boolean getIsFastRefreshToggleEnabled() {
        return settings.getBoolean(Settings.IS_FASE_REFRESH_TOGGLE_ENABLED, false);
    }

    public boolean getIsOtaDisabled() {
        return settings.getBoolean(Settings.DISABLE_OTA, false);
    }

    public boolean getRefreshScreenOnWake() {
        return settings.getBoolean(Settings.REFRESH_SCREEN_ON_WAKE, false);
    }
}
