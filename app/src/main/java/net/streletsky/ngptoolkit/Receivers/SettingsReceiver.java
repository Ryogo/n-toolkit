package net.streletsky.ngptoolkit.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.streletsky.ngptoolkit.Settings;
import net.streletsky.ngptoolkit.UI.BaseFragment;
import net.streletsky.ngptoolkit.UI.ScreenSettingsFragment;

public class SettingsReceiver extends BroadcastReceiver {
    public static final String INTENT_ACTION = "net.streletsky.ntoolkit.UPDATE_SETTINGS";
    public static final String PARAM_NAME = "SETTINGS_PARAM_NAME";
    public static final String VALUE_NAME = "SETTINGS_VALUE_NAME";

    @Override
    public void onReceive(Context context, Intent intent) {
        String paramName = intent.getStringExtra(PARAM_NAME);
        if (paramName == null) { return; }

        if (paramName.equalsIgnoreCase(Settings.IS_FAST_REFRESH_ENABLED)) {
            Boolean value = intent.getBooleanExtra(VALUE_NAME, false);
            new Settings(context).setIsFastRefreshEnabled(value);
            BaseFragment instance = ScreenSettingsFragment.GetInstance();
            if (instance != null) {
                instance.Refresh();
            }
        }
    }
}
