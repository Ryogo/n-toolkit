package net.streletsky.ngptoolkit;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;

import net.streletsky.ngptoolkit.UI.ActivationFragment;
import net.streletsky.ngptoolkit.UI.BaseFragment;
import net.streletsky.ngptoolkit.UI.GeneralSettingsFragment;
import net.streletsky.ngptoolkit.UI.ScreenSettingsFragment;
import net.streletsky.ngptoolkit.UI.ScreensaverSettingsFragment;
import net.streletsky.ngptoolkit.UI.StatusbarSettingsFragment;
import net.streletsky.ngptoolkit.UI.SystemInfoFragment;

public class MainActivity extends AppCompatActivity
    implements BaseFragment.OnFragmentInteractionListener {
    Settings settings;

    DrawerLayout drawerLayout;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Initialize();
    }

    void Initialize() {
        NavigationView navView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.activity_main);
        settings = new Settings(getApplicationContext());

        if (Utility.isModuleActive()) {
            setContent(R.id.nav_sys_info);
            navView.setCheckedItem(R.id.nav_sys_info);
            setTitle(navView.getMenu().findItem(R.id.nav_sys_info).getTitle());
        } else {
            setContent(ActivationFragment.id);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            return;
        }

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
        }

        navView.setNavigationItemSelectedListener(menuItem -> {
            setContent(menuItem.getItemId());
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START, false);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setContent(int itemId) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch(itemId) {
            case ActivationFragment.id:
                fragmentClass = ActivationFragment.class;
                break;
            case R.id.nav_general_settings:
                fragmentClass = GeneralSettingsFragment.class;
                break;
            case R.id.nav_statusbar_settings:
                fragmentClass = StatusbarSettingsFragment.class;
                break;
            case R.id.nav_screensaver_settings:
                fragmentClass = ScreensaverSettingsFragment.class;
                break;
            case R.id.nav_screen_settings:
                fragmentClass = ScreenSettingsFragment.class;
                break;
            case R.id.nav_sys_info:
                fragmentClass = SystemInfoFragment.class;
                break;
        }

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
        }
    }



    @Override
    public void onFragmentInteraction(Uri uri) { }
}
