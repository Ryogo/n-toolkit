package net.streletsky.ngptoolkit.UI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import net.streletsky.ngptoolkit.Logics.ScreenManager;
import net.streletsky.ngptoolkit.R;
import net.streletsky.ngptoolkit.Settings;

import java.lang.reflect.InvocationTargetException;

public class ScreenSettingsFragment extends BaseFragment {
    public static final int id = 0;

    public ScreenSettingsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_settings, container, false);
        Settings settings = new Settings(getActivity().getApplicationContext());

        Switch enableFastRefresh = (Switch) view.findViewById(R.id.enableFastRefresh);
        enableFastRefresh.setChecked(settings.getIsFastRefreshEnabled());
        enableFastRefresh.setOnCheckedChangeListener((compoundButton, value) -> {
            settings.setIsFastRefreshEnabled(value);

            if (value) {
                ScreenManager.FastRefresh(getView());
            } else {
                ScreenManager.FullRefresh(getView());
            }
        });

        return view;
    }

    @Override
    public void Refresh() {
        View view = getView();
        Settings settings = new Settings(getActivity().getApplicationContext());

        Switch enableFastRefresh = (Switch) view.findViewById(R.id.enableFastRefresh);
        enableFastRefresh.setChecked(settings.getIsFastRefreshEnabled());
    }
}
