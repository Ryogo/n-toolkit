package net.streletsky.ngptoolkit.UI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.streletsky.ngptoolkit.R;

public class ActivationFragment extends BaseFragment {
    public static final int id = -1;

    public ActivationFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activation, container, false);
    }
}
