package net.streletsky.ngptoolkit.UI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import net.streletsky.ngptoolkit.R;
import net.streletsky.ngptoolkit.Settings;

public class ScreensaverSettingsFragment extends BaseFragment {
    public static final int id = 0;

    public ScreensaverSettingsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screensaver_settings, container, false);
        Settings settings = new Settings(getActivity().getApplicationContext());

        Switch enableSleepScreen = (Switch) view.findViewById(R.id.enableSleepScreen);
        enableSleepScreen.setChecked(settings.getIsSleepScreenEnabled());
        enableSleepScreen.setOnCheckedChangeListener((compoundButton, value) -> {
            settings.setIsSleepScreenEnabled(value);

            Switch randomizeSleepScreensOrder = view.findViewById(R.id.randomizeSleepScreensOrder);
            randomizeSleepScreensOrder.setEnabled(value);

            Switch refreshScreenOnWake = view.findViewById(R.id.refreshScreenOnWake);
            refreshScreenOnWake.setEnabled(value);
        });

        Switch refreshScreenOnWake = (Switch) view.findViewById(R.id.refreshScreenOnWake);
        refreshScreenOnWake.setChecked(settings.getRefreshScreenOnWake());
        refreshScreenOnWake.setOnCheckedChangeListener((compoundButton, value) -> settings.setRefreshScreenOnWake(value));

        Switch randomizeSleepScreensOrder = (Switch) view.findViewById(R.id.randomizeSleepScreensOrder);
        randomizeSleepScreensOrder.setChecked(settings.getRandomizeSleepScreenOrder());
        randomizeSleepScreensOrder.setOnCheckedChangeListener((compoundButton, value) -> settings.setRandomizeSleepScreenOrder(value));

        return view;
    }
}
