package net.streletsky.ngptoolkit.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.ImageView;

import net.streletsky.ngptoolkit.Logics.ScreenManager;

public class ScreensaverView extends ImageView {
    int updateMode;
    int drawCount;
    OnDrawListener onDrawListener;

    public ScreensaverView(Context context) {
        super(context);
    }

    public void setUpdateMode(int updateMode) {
        this.updateMode = updateMode;
    }

    public void addOnDrawListener(OnDrawListener onDrawListener) {
        this.onDrawListener = onDrawListener;
    }

    public void removeOnDrawListener() {
        this.onDrawListener = null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCount++;
        if (onDrawListener != null){
            onDrawListener.onDraw(drawCount);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    public void invalidate() {
        if (updateMode == 0) {
            ScreenManager.FullRefresh(this);
        } else {
            ScreenManager.Refresh(this, updateMode);
        }
    }

    public interface OnDrawListener {
        void onDraw(int drawCount);
    }
}