package net.streletsky.ngptoolkit.UI;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import net.streletsky.ngptoolkit.R;
import net.streletsky.ngptoolkit.Settings;

public class StatusbarSettingsFragment extends BaseFragment {
    public static final int id = 0;

    public StatusbarSettingsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statusbar_settings, container, false);
        Settings settings = new Settings(getActivity().getApplicationContext());

        if (isStatusBarFwRequirementsMet()) {
            Switch enableStatusBar = (Switch) view.findViewById(R.id.enableStatusBar);
            enableStatusBar.setChecked(settings.getIsStatusBarEnabled());
            enableStatusBar.setOnCheckedChangeListener((compoundButton, value) -> {
                settings.setIsStatusBarEnabled(value);
                if (value) {
                    Switch removeStatusBar = (Switch) view.findViewById(R.id.removeStatusBar);
                    removeStatusBar.setChecked(false);
                }
            });
        } else {
            Switch enableStatusBar = (Switch) view.findViewById(R.id.enableStatusBar);
            enableStatusBar.setChecked(false);
            enableStatusBar.setEnabled(false);
        }

        Switch removeStatusBar = (Switch) view.findViewById(R.id.removeStatusBar);
        removeStatusBar.setChecked(settings.getRemoveStatusBar());
        removeStatusBar.setOnCheckedChangeListener((compoundButton, value) -> {
            settings.setRemoveStatusBar(value);
            if (value) {
                Switch enableStatusBar = (Switch) view.findViewById(R.id.enableStatusBar);
                enableStatusBar.setChecked(false);
            }
        });

        Switch hideStatusBarProfile = (Switch) view.findViewById(R.id.hideProfile);
        hideStatusBarProfile.setChecked(settings.getHideStatusBarProfile());
        hideStatusBarProfile.setOnCheckedChangeListener((compoundButton, value) -> settings.setHideStatusBarProfile(value));

        Switch hideStatusBarSettings = (Switch) view.findViewById(R.id.hideSettingsLink);
        hideStatusBarSettings.setChecked(settings.getHideStatusBarSettings());
        hideStatusBarSettings.setOnCheckedChangeListener((compoundButton, value) -> settings.setHideStatusBarSettings(value));

        Switch replaceStatusBarSettingsAction = (Switch) view.findViewById(R.id.replaceSettingsAction);
        replaceStatusBarSettingsAction.setChecked(settings.getReplaceStatusBarSettingsAction());
        replaceStatusBarSettingsAction.setOnCheckedChangeListener((compoundButton, value) -> settings.setReplaceStatusBarSettingsAction(value));

        Switch showFastRefreshToggle = (Switch) view.findViewById(R.id.showFastRefreshToggle);
        showFastRefreshToggle.setChecked(settings.getIsFastRefreshToggleEnabled());
        showFastRefreshToggle.setOnCheckedChangeListener((compoundButton, value) -> settings.setIsFastRefreshToggleEnabled(value));


        return view;
    }

    boolean isStatusBarFwRequirementsMet() {
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo("com.nook.partner", 0);
            if (packageInfo.versionCode < 757) { return false; }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }

        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo("bn.ereader", 0);
            if (packageInfo.versionCode < 757) { return false; }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }

        return true;
    }
}
