package net.streletsky.ngptoolkit.UI;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.net.Uri;

public class BaseFragment extends Fragment {
    static BaseFragment instance;
    protected OnFragmentInteractionListener listener;

    public static BaseFragment GetInstance() {
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            listener = (OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = this;
        Refresh();
    }

    @Override
    public void onPause() {
        super.onPause();
        instance = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void Refresh() { }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
