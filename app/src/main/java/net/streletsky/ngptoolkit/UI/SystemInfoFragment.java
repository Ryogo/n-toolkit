package net.streletsky.ngptoolkit.UI;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.streletsky.ngptoolkit.R;

public class SystemInfoFragment extends BaseFragment {
    public static final int id = -1;

    public SystemInfoFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_system_info, container, false);

        initialize(view);

        return view;
    }

    void initialize(View view) {
        TextView firmwareVersion = (TextView)view.findViewById(R.id.firmwareVersion);
        firmwareVersion.setText(Build.VERSION.INCREMENTAL.split("_")[0]);

        TextView toolkitVersion = (TextView)view.findViewById(R.id.toolkitVersion);
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo("net.streletsky.ngptoolkit", 0);
            toolkitVersion.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            toolkitVersion.setText("not installed");
        }

        TextView partnerVersion = (TextView)view.findViewById(R.id.partnerVersion);
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo("com.nook.partner", 0);
            partnerVersion.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            partnerVersion.setText("not installed");
        }

        TextView ereaderVersion = (TextView)view.findViewById(R.id.ereaderVersion);
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo("bn.ereader", 0);
            ereaderVersion.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            ereaderVersion.setText("not installed");
        }
    }
}
