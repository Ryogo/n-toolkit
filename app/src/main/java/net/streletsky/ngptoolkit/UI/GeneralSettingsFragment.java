package net.streletsky.ngptoolkit.UI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import net.streletsky.ngptoolkit.R;
import net.streletsky.ngptoolkit.Settings;

public class GeneralSettingsFragment extends BaseFragment {
    public static final int id = 0;

    public GeneralSettingsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_settings, container, false);
        Settings settings = new Settings(getActivity().getApplicationContext());

        Switch disableDefaultLauncher = (Switch)view.findViewById(R.id.disableDefaultLauncher);
        disableDefaultLauncher.setChecked(settings.getIsDefaultLauncherDisabled());
        disableDefaultLauncher.setOnCheckedChangeListener((compoundButton, value) -> settings.setIsDefaultLauncherDisabled(value));

        Switch homeButtonLongPress = (Switch)view.findViewById(R.id.homeLongPress);
        homeButtonLongPress.setChecked(settings.getHomeButtonLongPress());
        homeButtonLongPress.setOnCheckedChangeListener((compoundButton, value) -> settings.setHomeButtonLongPress(value));

        Switch mtpFullAccess = (Switch)view.findViewById(R.id.mtpFullAccess);
        mtpFullAccess.setChecked(settings.getMtpFullAccess());
        mtpFullAccess.setOnCheckedChangeListener((compoundButton, value) -> settings.setMtpFullAccess(value));

        Switch usbWakeup = (Switch)view.findViewById(R.id.usbWakeup);
        usbWakeup.setChecked(settings.getUsbWakeUp());
        usbWakeup.setOnCheckedChangeListener((compoundButton, value) -> settings.setUsbWakeUp(value));

        Switch disableLowBatteryWarning = (Switch)view.findViewById(R.id.disableLowBatteryWarning);
        disableLowBatteryWarning.setChecked(settings.getIsLowBatteryWarningDisabled());
        disableLowBatteryWarning.setOnCheckedChangeListener((compoundButton, value) -> settings.setIsLowBatteryWarningDisabled(value));

        Switch disableOta = (Switch)view.findViewById(R.id.disableOta);
        disableOta.setChecked(settings.getIsOtaDisabled());
        disableOta.setOnCheckedChangeListener((compoundButton, value) -> settings.setIsOtaDisabled(value));

        return view;
    }
}
