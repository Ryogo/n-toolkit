package net.streletsky.ngptoolkit;

class Utility {
    static Settings settings;

    public static boolean isModuleActive() {
        return false;
    }

    public static Settings getSettings() {
        if (settings == null) { settings = new Settings(null); }

        return settings;
    }
}
