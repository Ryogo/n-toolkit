package net.streletsky.ngptoolkit.Utils;

import android.util.Log;

public class SystemPropertiesProxy {
    private SystemPropertiesProxy() { }

    public static String get(String key) {
        String value = null;

        try {
            value = (String) Class.forName("android.os.SystemProperties")
                    .getMethod("get", String.class).invoke(null, key);
        } catch (Exception e) {
            Log.d("[NTOOLKIT]", "getprop failed");
        }

        return value;
    }

    public static String get(String key, String defaultValue) {
        String value = defaultValue;

        try {
            value = (String) Class.forName("android.os.SystemProperties")
                    .getMethod("get", String.class, String.class).invoke(null, key, defaultValue);
        } catch (Exception e) {
            Log.d("[NTOOLKIT]", "getprop failed");
        }

        return value;
    }

    public static void set(String key, String value) {
        try {
            value = (String) Class.forName("android.os.SystemProperties")
                    .getMethod("set", String.class, String.class).invoke(null, key, value);
        } catch (Exception e) {
            Log.d("[NTOOLKIT]", "setprop failed");
        }
    }
}