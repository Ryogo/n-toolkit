package net.streletsky.ngptoolkit;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {
    public static final String CONFIG_ID = "settings";
    public static final String MTP_FULL_ACCESS = "mtpFullAccess";
    public static final String HOME_BUTTON_LONG_PRESS = "homeButtonLongPress";
    public static final String USB_WAKEUP = "usbWakeup";
    public static final String ENABLE_STATUS_BAR = "enableStatusBar";
    public static final String REMOVE_STATUS_BAR = "removeStatusBar";
    public static final String IS_SLEEP_SCREEN_ENABLED = "isSleepScreenEnabled";
    public static final String DISABLE_DEFAULT_LAUNCHER = "disableDefaultLauncher";
    public static final String RANDOMIZE_SLEEP_SCREENS = "randomizeSleepScreens";
    public static final String HIDE_STATUSBAR_PROFILE = "hideStatusBarProfile";
    public static final String HIDE_STATUSBAR_SETTINGS = "hideStatusBarSettings";
    public static final String REPLACE_STATUSBAR_SETTINGS_ACTION = "replaceStatusBarSettingsAction";
    public static final String IS_FAST_REFRESH_ENABLED = "enableFastRefresh";
    public static final String DISABLE_LOW_BATTERY_WARNING = "disableLowBatteryWarning";
    public static final String IS_FASE_REFRESH_TOGGLE_ENABLED = "IS_FAST_REFRESH_TOGGLE_ENABLED";
    public static final String DISABLE_OTA = "DISABLE_OTA";
    public static final String REFRESH_SCREEN_ON_WAKE = "REFRESH_SCREEN_ON_WAKE";

    SharedPreferences settings;

    public Settings(Context context) {
        settings = context.getSharedPreferences(CONFIG_ID, Context.MODE_WORLD_READABLE);
    }

    public boolean getMtpFullAccess() {
        return settings.getBoolean(MTP_FULL_ACCESS, false);
    }

    public void setMtpFullAccess(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(MTP_FULL_ACCESS, value);
        editor.apply();
    }

    public boolean getHomeButtonLongPress() {
        return settings.getBoolean(HOME_BUTTON_LONG_PRESS, false);
    }

    public void setHomeButtonLongPress(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(HOME_BUTTON_LONG_PRESS, value);
        editor.apply();
    }

    public boolean getUsbWakeUp() {
        return settings.getBoolean(USB_WAKEUP, false);
    }

    public void setUsbWakeUp(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(USB_WAKEUP, value);
        editor.apply();
    }

    public boolean getRemoveStatusBar() {
        return settings.getBoolean(REMOVE_STATUS_BAR, false);
    }

    public boolean getIsStatusBarEnabled() {
        return settings.getBoolean(Settings.ENABLE_STATUS_BAR, false);
    }

    public void setIsStatusBarEnabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ENABLE_STATUS_BAR, value);
        editor.apply();
    }

    public void setRemoveStatusBar(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(REMOVE_STATUS_BAR, value);
        editor.apply();
    }

    public boolean getIsSleepScreenEnabled() {
        return settings.getBoolean(IS_SLEEP_SCREEN_ENABLED, false);
    }

    public void setIsSleepScreenEnabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(IS_SLEEP_SCREEN_ENABLED, value);
        editor.apply();
    }

    public boolean getIsDefaultLauncherDisabled() {
        return settings.getBoolean(DISABLE_DEFAULT_LAUNCHER, false);
    }

    public void setIsDefaultLauncherDisabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(DISABLE_DEFAULT_LAUNCHER, value);
        editor.apply();
    }

    public boolean getRandomizeSleepScreenOrder() {
        return settings.getBoolean(RANDOMIZE_SLEEP_SCREENS, false);
    }

    public void setRandomizeSleepScreenOrder(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(RANDOMIZE_SLEEP_SCREENS, value);
        editor.apply();
    }

    public boolean getHideStatusBarProfile() {
        return settings.getBoolean(HIDE_STATUSBAR_PROFILE, false);
    }

    public void setHideStatusBarProfile(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(HIDE_STATUSBAR_PROFILE, value);
        editor.apply();
    }

    public boolean getHideStatusBarSettings() {
        return settings.getBoolean(HIDE_STATUSBAR_SETTINGS, false);
    }

    public void setHideStatusBarSettings(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(HIDE_STATUSBAR_SETTINGS, value);
        editor.apply();
    }

    public boolean getReplaceStatusBarSettingsAction() {
        return settings.getBoolean(REPLACE_STATUSBAR_SETTINGS_ACTION, false);
    }

    public void setReplaceStatusBarSettingsAction(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(REPLACE_STATUSBAR_SETTINGS_ACTION, value);
        editor.apply();
    }

    public boolean getIsFastRefreshEnabled() {
        return settings.getBoolean(IS_FAST_REFRESH_ENABLED, false);
    }

    public void setIsFastRefreshEnabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(IS_FAST_REFRESH_ENABLED, value);
        editor.apply();
    }

    public boolean getIsLowBatteryWarningDisabled() {
        return settings.getBoolean(DISABLE_LOW_BATTERY_WARNING, false);
    }

    public void setIsLowBatteryWarningDisabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(DISABLE_LOW_BATTERY_WARNING, value);
        editor.apply();
    }

    public boolean getIsFastRefreshToggleEnabled() {
        return settings.getBoolean(IS_FASE_REFRESH_TOGGLE_ENABLED, false);
    }

    public void setIsFastRefreshToggleEnabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(IS_FASE_REFRESH_TOGGLE_ENABLED, value);
        editor.apply();
    }

    public boolean getIsOtaDisabled() {
        return settings.getBoolean(DISABLE_OTA, false);
    }

    public void setIsOtaDisabled(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(DISABLE_OTA, value);
        editor.apply();
    }

    public boolean getRefreshScreenOnWake() {
        return settings.getBoolean(REFRESH_SCREEN_ON_WAKE, false);
    }

    public void setRefreshScreenOnWake(boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(REFRESH_SCREEN_ON_WAKE, value);
        editor.apply();
    }
}