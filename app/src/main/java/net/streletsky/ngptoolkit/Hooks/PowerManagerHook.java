package net.streletsky.ngptoolkit.Hooks;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;

import net.streletsky.ngptoolkit.Logics.ScreensaverManager;
import net.streletsky.ngptoolkit.XSettings;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

public class PowerManagerHook implements IXposedHookZygoteInit {
    private static final String POWER_MANAGER_SERVICE  = "com.android.server.power.PowerManagerService";

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        XSettings settings = new XSettings();
        if (settings.getUsbWakeUp()) {
            Class<?> powerManagerServiceClass = XposedHelpers.findClass(POWER_MANAGER_SERVICE, null);
            XposedBridge.hookAllMethods(powerManagerServiceClass, "shouldWakeUpWhenPluggedOrUnpluggedLocked", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(false);
                }
            });
        }

        if (settings.getIsSleepScreenEnabled()) {
            Class<?> powerManagerServiceClass = XposedHelpers.findClass(POWER_MANAGER_SERVICE, null);
            XposedBridge.hookAllMethods(powerManagerServiceClass, "handleUserActivityTimeout", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    final Context context = (Context) XposedHelpers.getObjectField(param.thisObject, "mContext");
                    final Handler handler = (Handler) XposedHelpers.getObjectField(param.thisObject, "mHandler");
                    ScreensaverManager sleepScreen = new ScreensaverManager(context, handler);
                    sleepScreen.show(() -> XposedHelpers.callMethod(param.thisObject, "goToSleep", SystemClock.uptimeMillis(), 2)); // GO_TO_SLEEP_REASON_TIMEOUT

                    param.setResult(null);
                }
            });
        }
    }
}