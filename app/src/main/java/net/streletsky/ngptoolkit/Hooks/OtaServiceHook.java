package net.streletsky.ngptoolkit.Hooks;

import net.streletsky.ngptoolkit.XSettings;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class OtaServiceHook implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(final LoadPackageParam packageParam) throws Throwable {
        if (!packageParam.packageName.equals("com.nook.partner")) { return; }

        final XSettings settings = new XSettings();
        if (!settings.getIsOtaDisabled()) { return; }

        Class<?> bootCompleteReceiverClass = XposedHelpers.findClass("com.nook.partner.otamanager.OtaIntentService.BootCompleteReceiver", packageParam.classLoader);
        XposedBridge.hookAllMethods(bootCompleteReceiverClass, "onReceive", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(null);
            }
        });

        Class<?> connectivityChangeReceiverClass = XposedHelpers.findClass("com.nook.partner.otamanager.OtaIntentService.ConnectivityChangeReceiver", packageParam.classLoader);
        XposedBridge.hookAllMethods(connectivityChangeReceiverClass, "onReceive", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(null);
            }
        });

        Class<?> screenOffReceiverClass = XposedHelpers.findClass("com.nook.partner.otamanager.OtaIntentService.ScreenOffReceiver", packageParam.classLoader);
        XposedBridge.hookAllMethods(screenOffReceiverClass, "onReceive", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(null);
            }
        });
    }
}