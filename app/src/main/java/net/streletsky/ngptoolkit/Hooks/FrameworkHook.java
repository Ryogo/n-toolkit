package net.streletsky.ngptoolkit.Hooks;

import android.graphics.Rect;

import net.streletsky.ngptoolkit.XSettings;

import java.util.ArrayList;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

public class FrameworkHook implements IXposedHookZygoteInit {
    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        XSettings settings = new XSettings();
        if (!settings.getIsFastRefreshEnabled()) { return; }

        Class<?> viewRootClass = XposedHelpers.findClass("android.view.ViewRootImpl", null);

        XposedBridge.hookAllMethods(viewRootClass, "invalidate", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                if (param.args.length == 0 || (int)param.args[0] == 5) {
                    Rect mDirty = (Rect)XposedHelpers.getObjectField(param.thisObject, "mDirty");
                    int mWidth = (int)XposedHelpers.getObjectField(param.thisObject, "mWidth");
                    int mHeight = (int)XposedHelpers.getObjectField(param.thisObject, "mHeight");
                    mDirty.set(0, 0, mWidth, mHeight);
                    ArrayList<int[]> mDirtyGroup = (ArrayList<int[]>)XposedHelpers.getObjectField(param.thisObject, "mDirtygroup");
                    mDirtyGroup.clear();
                    mDirtyGroup.add(new int[]{0, 0, mWidth, mHeight, 268437764});
                    XposedHelpers.callMethod(param.thisObject, "scheduleTraversals");
                    param.setResult(0);
                }
            }
        });
    }
}