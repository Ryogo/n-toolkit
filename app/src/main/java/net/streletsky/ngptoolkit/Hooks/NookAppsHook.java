package net.streletsky.ngptoolkit.Hooks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.streletsky.ngptoolkit.Logics.ScreenManager;
import net.streletsky.ngptoolkit.Receivers.SettingsReceiver;
import net.streletsky.ngptoolkit.Settings;
import net.streletsky.ngptoolkit.XSettings;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class NookAppsHook implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam packageParam) throws Throwable {
        if (!packageParam.packageName.equals("android") && !packageParam.packageName.equals("com.nook.partner") && !packageParam.packageName.equals("bn.ereader")) { return; }

        // disable "Sign in to network"
        if (packageParam.packageName.equals("android")) {
            Class<?> captivePortalTrackerClass = XposedHelpers.findClass("android.net.CaptivePortalTracker", packageParam.classLoader);
            XposedBridge.hookAllMethods(captivePortalTrackerClass, "isCaptivePortal", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    boolean mIsCaptivePortalCheckEnabled = XposedHelpers.getBooleanField(param.thisObject, "mIsCaptivePortalCheckEnabled");

                    boolean result = false;
                    if (mIsCaptivePortalCheckEnabled) {
                        result = isCaptivePortal((InetAddress)param.args[0]);
                    }

                    param.setResult(result);
                }
            });
        }

        final XSettings settings = new XSettings();
        if (settings.getIsSleepScreenEnabled() && packageParam.packageName.equals("com.nook.partner")) {
            Class<?> screenSaverClass = XposedHelpers.findClass("com.nook.partner.screensaver.ScreenSaver", packageParam.classLoader);
            XposedBridge.hookAllMethods(screenSaverClass, "setScreenSaver", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(null);
                }
            });

            Class<?> lockScreenClass = null;
            try {
                lockScreenClass = XposedHelpers.findClass("com.nook.partner.lockscreen.LockScreenManager", packageParam.classLoader);
            } catch (XposedHelpers.ClassNotFoundError cnf) { }
            if (lockScreenClass != null) {
                XposedBridge.hookAllMethods(lockScreenClass, "addLockerScreen", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.setResult(null);
                    }
                });

                XposedBridge.hookAllMethods(lockScreenClass, "cancelLockScreen", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.setResult(null);
                    }
                });
            }
        }

        if (settings.getIsStatusBarEnabled()) {
            if (packageParam.packageName.equals("bn.ereader")) {
                final Class<?> epdUtilsClass = XposedHelpers.findClass("com.nook.lib.epdcommon.EpdUtils", packageParam.classLoader);

                XposedBridge.hookAllMethods(epdUtilsClass, "initStatusBarService", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.setResult(0);

                        Context context = (Context) param.args[0];
                        if (context != null) {
                            Intent intent = new Intent("com.nook.intent.action.statusbar");
                            context.startService(intent);
                        }
                    }
                });
            }

            if (packageParam.packageName.equals("com.nook.partner")) {
                final Class<?> statusBarClass = XposedHelpers.findClass("com.nook.partner.statusbar.StatusBar", packageParam.classLoader);

                XposedBridge.hookAllMethods(statusBarClass, "progressBegin", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.setResult(0);
                    }
                });

                XposedBridge.hookAllMethods(statusBarClass, "progressEnd", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.setResult(0);
                    }
                });

                final Class<?> statusBarServiceClass = XposedHelpers.findClass("com.nook.partner.statusbar.StatusBarService", packageParam.classLoader);
                XposedBridge.hookAllMethods(statusBarServiceClass, "initStatusBarService", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        Object mPanel = XposedHelpers.getStaticObjectField(statusBarServiceClass, "mPanel");
                        XposedHelpers.callMethod(mPanel, "update", 0, "User");
                        XposedHelpers.callMethod(mPanel, "show");
                    }
                });
            }
        }

        if (settings.getIsLowBatteryWarningDisabled() && packageParam.packageName.equals("com.nook.partner")) {
            final Class<?> batteryIconClass = XposedHelpers.findClass("com.nook.partner.statusbar.BatteryIcon", packageParam.classLoader);
            XposedBridge.hookAllMethods(batteryIconClass, "isNeedToControlBatteryNotification", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(false);
                }
            });
        }

        if ((settings.getHideStatusBarProfile() || settings.getReplaceStatusBarSettingsAction() || settings.getIsFastRefreshToggleEnabled()) && packageParam.packageName.equals("com.nook.partner")) {
            final Class<?> quickSettingsClass = XposedHelpers.findClass("com.nook.partner.statusbar.QuickSettings", packageParam.classLoader);
            XposedBridge.hookAllMethods(quickSettingsClass, "init", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(final MethodHookParam param) throws Throwable {
                    ViewGroup rootView = (ViewGroup)param.thisObject;
                    Context context = rootView.getContext();

                    if (settings.getHideStatusBarProfile()) {
                        int id = rootView.getResources().getIdentifier("profile_area", "id", context.getPackageName());
                        if (id != 0) {
                            View profileArea = rootView.findViewById(id);
                            profileArea.setVisibility(View.GONE);
                        }
                    }
                    if (settings.getHideStatusBarSettings()) {
                        int id = rootView.getResources().getIdentifier("all_settings", "id", context.getPackageName());
                        if (id != 0) {
                            View allSettings = rootView.findViewById(id);
                            allSettings.setVisibility(View.GONE);
                        }
                    } else if (settings.getReplaceStatusBarSettingsAction()) {
                        int id = rootView.getResources().getIdentifier("see_all_text", "id", context.getPackageName());
                        if (id != 0) {
                            View settingsBtn = rootView.findViewById(id);
                            settingsBtn.setOnClickListener(view -> {
                                Object statusBar = XposedHelpers.getObjectField(param.thisObject, "mStatusBar");
                                XposedHelpers.callMethod(statusBar, "expand", 1200, true);
                                Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                context.startActivity(intent);
                                XposedHelpers.callMethod(param.thisObject, "setVisibility", View.GONE);
                            });
                        }
                    }

                    if (settings.getIsFastRefreshToggleEnabled()) {
                        addFastRefreshToggle(rootView);
                    }
                }
            });
        }
    }

    void addFastRefreshToggle(Object viewObject) {
        final ViewGroup view = (ViewGroup)viewObject;
        Context context = view.getContext();

        boolean secondaryView = false;
        int id = view.getResources().getIdentifier("night_mode_settings", "id", context.getPackageName());
        if (id == 0) {
            view.getResources().getIdentifier("glowIcon", "id", context.getPackageName());
            secondaryView = true;
        }

        if (id == 0) { return; }

        View anchorView = view.findViewById(id);
        if (anchorView == null) { return; }

        ViewGroup parentView = (ViewGroup)anchorView.getParent();
        if (secondaryView) {
            anchorView = parentView;
            parentView = (ViewGroup)anchorView.getParent();
        }

        int index = parentView.indexOfChild(anchorView);

        DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();

        LinearLayout container = new LinearLayout(context);
        container.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        container.setPadding(dpToPx(62, displayMetrics), dpToPx(12, displayMetrics), dpToPx(61, displayMetrics), dpToPx(12, displayMetrics));

        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setText("Fast refresh mode");
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        container.addView(textView);

        CheckBox checkBox = new CheckBox(context);
        checkBox.setButtonDrawable(view.getResources().getIdentifier("epd_checkbox", "drawable", context.getPackageName()));
        checkBox.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        checkBox.setChecked(new XSettings().getIsFastRefreshEnabled());
        checkBox.setOnCheckedChangeListener((compoundButton, value) -> {
            if (value) {
                ScreenManager.FastRefresh(view);
            } else {
                ScreenManager.FullRefresh(view);
            }
            Intent intent = new Intent();
            intent.setAction(SettingsReceiver.INTENT_ACTION);
            intent.putExtra(SettingsReceiver.PARAM_NAME, Settings.IS_FAST_REFRESH_ENABLED);
            intent.putExtra(SettingsReceiver.VALUE_NAME, value);
            intent.addFlags(android.content.Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            compoundButton.getContext().sendBroadcast(intent);
        });
        container.addView(checkBox);

        parentView.addView(container, index + 1);

        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            checkBox.setChecked(new XSettings().getIsFastRefreshEnabled());
        });
    }

    public static int dpToPx(int dp, DisplayMetrics metrics) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    private static final int SOCKET_TIMEOUT_MS = 1000;
    private boolean isCaptivePortal(InetAddress server) {
        HttpURLConnection urlConnection = null;
        String mUrl = "http://" + server.getHostAddress() + "/generate_204";
        try {
            URL url = new URL(mUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setConnectTimeout(SOCKET_TIMEOUT_MS);
            urlConnection.setReadTimeout(SOCKET_TIMEOUT_MS);
            urlConnection.setUseCaches(false);
            urlConnection.getInputStream();

            return urlConnection.getResponseCode() != 204;
        } catch (IOException e) {
            return false;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
}