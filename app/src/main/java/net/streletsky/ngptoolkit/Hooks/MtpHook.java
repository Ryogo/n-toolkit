package net.streletsky.ngptoolkit.Hooks;

import android.os.Environment;

import net.streletsky.ngptoolkit.Utils.SystemPropertiesProxy;
import net.streletsky.ngptoolkit.XSettings;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

public class MtpHook implements IXposedHookZygoteInit {
    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        setUsbMode();

        XSettings settings = new XSettings();
        if (!settings.getMtpFullAccess()) { return; }

        Class<?> mtpDatabaseClass = XposedHelpers.findClass("android.mtp.MtpDatabase", null);
        XposedBridge.hookAllConstructors(mtpDatabaseClass, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                String[] subDirectories = (String[])param.args[3];
                if (subDirectories.length == 0 || !subDirectories[0].endsWith("/update.zip")) { return; }

                subDirectories[3] = Environment.getExternalStoragePublicDirectory("/").getPath();
            }
        });
    }

    void setUsbMode() {
        String hardware = SystemPropertiesProxy.get("ro.product.model");
        if (!hardware.equals("BNRV520") && !hardware.equals("BNRV700")) { return; }

        String usbMode = SystemPropertiesProxy.get("persist.sys.usb.config", "mass_storage");
        XSettings settings = new XSettings();
        if (settings.getMtpFullAccess()) {
            if (!usbMode.contains("mass_storage") && usbMode.contains("mtp")) { return; }

            String newUsbMode = "mtp";
            if (usbMode.contains("adb")) {
                newUsbMode += ",adb";
            }

            SystemPropertiesProxy.set("persist.sys.usb.config", newUsbMode);
        } else {
            if (usbMode.contains("mass_storage") && !usbMode.contains("mtp")) { return; }

            String newUsbMode = "mass_storage";
            if (usbMode.contains("adb")) {
                newUsbMode += ",adb";
            }

            SystemPropertiesProxy.set("persist.sys.usb.config", newUsbMode);
        }
    }
}