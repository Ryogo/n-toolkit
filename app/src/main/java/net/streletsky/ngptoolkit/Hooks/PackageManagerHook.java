package net.streletsky.ngptoolkit.Hooks;

import android.content.Intent;
import android.content.pm.ResolveInfo;

import net.streletsky.ngptoolkit.XSettings;

import java.util.List;
import java.util.Set;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

public class PackageManagerHook implements IXposedHookZygoteInit {
    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        XSettings settings = new XSettings();
        if (!settings.getIsDefaultLauncherDisabled()) { return; }

        Class<?> pmServiceClass = XposedHelpers.findClass("com.android.server.pm.PackageManagerService", null);

        XposedBridge.hookAllMethods(pmServiceClass, "queryIntentActivities", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                Intent intent = (Intent)param.args[0];
                String action = intent.getAction();
                if (action != "android.intent.action.MAIN") { return; }

                Set<String> categories = intent.getCategories();
                if (categories == null || categories.size() != 1 || categories.iterator().next() != "android.intent.category.HOME") { return; }

                List<ResolveInfo> infos = (List<ResolveInfo>)param.getResult();
                if (infos.size() < 2) { return; }

                ResolveInfo defaultLauncherInfo = null;
                for (ResolveInfo info : infos) {
                    if (info.activityInfo.packageName != "com.nook.partner") { continue; }
                    defaultLauncherInfo = info;
                    break;
                }
                infos.remove(defaultLauncherInfo);

                param.setResult(infos);
            }
        });
    }
}
