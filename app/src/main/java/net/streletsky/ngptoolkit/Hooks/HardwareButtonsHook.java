package net.streletsky.ngptoolkit.Hooks;

import android.content.Context;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.ViewConfiguration;

import net.streletsky.ngptoolkit.Logics.FrontlightManager;
import net.streletsky.ngptoolkit.Logics.PowerManagerEx;
import net.streletsky.ngptoolkit.Logics.ScreensaverManager;
import net.streletsky.ngptoolkit.XSettings;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class HardwareButtonsHook implements IXposedHookLoadPackage {
    private static final Object syncObject = new Object();
    private static boolean pengingAction;

    @Override
    public void handleLoadPackage(final LoadPackageParam packageParam) throws Throwable {
        if (!packageParam.packageName.equals("android")) { return; }

        final XSettings settings = new XSettings();
        if (settings.getIsSleepScreenEnabled()) {
            Class<?> screenClass = XposedHelpers.findClass("ntx.fb.Screen", packageParam.classLoader);
            XposedBridge.hookAllMethods(screenClass, "draw", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(false);
                }
            });
        }

        if (settings.getIsSleepScreenEnabled() || settings.getHomeButtonLongPress() || settings.getHomeButtonLongPress()) {
            Class<?> phoneWindowManager = XposedHelpers.findClass("com.android.internal.policy.impl.PhoneWindowManager", packageParam.classLoader);
            XposedBridge.hookAllMethods(phoneWindowManager, "interceptKeyBeforeQueueing", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    KeyEvent event = (KeyEvent) param.args[0];
                    boolean isKeyDown = event.getAction() == KeyEvent.ACTION_DOWN;
                    synchronized (syncObject) {
                        if (isKeyDown) {
                            pengingAction = true;
                        }
                    }

                    final Context context = (Context) XposedHelpers.getObjectField(param.thisObject, "mContext");
                    final Handler handler = (Handler) XposedHelpers.getObjectField(param.thisObject, "mHandler");

                    if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                        if (!settings.getIsSleepScreenEnabled()) { return; }

                        param.setResult(0);

                        if (isKeyDown) {
                            final PowerManagerEx powerManager = new PowerManagerEx(param.thisObject);
                            handler.postDelayed(() -> {
                                synchronized (syncObject) {
                                    if (!pengingAction) {
                                        return;
                                    }
                                    pengingAction = false;
                                }

                                powerManager.showPowerDialog();
                            }, ViewConfiguration.getGlobalActionKeyTimeout());
                        } else {
                            synchronized (syncObject) {
                                if (!pengingAction) { return; }
                                pengingAction = false;
                            }

                            final PowerManagerEx powerManager = new PowerManagerEx(param.thisObject);
                            ScreensaverManager sleepScreen = new ScreensaverManager(context, handler);
                            if (powerManager.isScreenOn()) {
                                sleepScreen.show(() -> powerManager.goToSleep());
                            } else {
                                powerManager.wakeUp();
                            }
                        }
                    } else if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
                        if (!settings.getHomeButtonLongPress()) { return; }

                        if (isKeyDown) {
                            handler.postDelayed(() -> {
                                synchronized (syncObject) {
                                    if (!pengingAction) { return; }
                                    pengingAction = false;
                                }

                                FrontlightManager frontlightManager = new FrontlightManager(context);
                                if (frontlightManager.isBrightnessOverridenByWindowManager()) { return; }

                                boolean isFrontlightOn = frontlightManager.isFrontlightOn();
                                frontlightManager.setFrontlightState(!isFrontlightOn);
                            }, ViewConfiguration.getGlobalActionKeyTimeout());
                        } else {
                            synchronized (syncObject) {
                                if (!pengingAction) {
                                    param.setResult(0);
                                    return;
                                }
                                pengingAction = false;
                            }
                        }
                    }
                }
            });
        }
    }
}