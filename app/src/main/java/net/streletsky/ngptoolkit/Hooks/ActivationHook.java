package net.streletsky.ngptoolkit.Hooks;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class ActivationHook implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(final LoadPackageParam packageParam) throws Throwable {
        if (!packageParam.packageName.equals("net.streletsky.ngptoolkit")) { return; }

        Class<?> utilityClass = XposedHelpers.findClass("net.streletsky.ngptoolkit.Utility", packageParam.classLoader);
        XposedBridge.hookAllMethods(utilityClass, "isModuleActive", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(true);
            }
        });
    }
}
