package net.streletsky.ngptoolkit.Hooks;

import android.content.res.Resources;
import android.content.res.XResources;
import android.util.TypedValue;

import net.streletsky.ngptoolkit.XSettings;

import java.lang.reflect.Array;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

public class StatusBarHook implements IXposedHookZygoteInit, IXposedHookInitPackageResources {
    @Override
    public void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        XSettings settings = new XSettings();
        if (!settings.getRemoveStatusBar()) { return; }		
		
        Resources resources = Resources.getSystem();
        int id = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (id == 0) { return; }

        XResources.setSystemWideReplacement(id, new XResources.DimensionReplacement(0, TypedValue.COMPLEX_UNIT_PX));
    }

    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam resourcesParam) throws Throwable {
        if (!resourcesParam.packageName.equals("com.android.settings")) { return; }

        resourcesParam.res.setReplacement("com.android.settings", "array", "screen_timeout_entries", new String[] {
                "15 Seconds",
                "30 Seconds",
                "60 Seconds",
                "2 Minutes",
                "5 Minutes",
                "15 Minutes",
                "30 Minutes",
                "60 Minutes",
                "Never"
        });

        resourcesParam.res.setReplacement("com.android.settings", "array", "screen_timeout_values", new String[] {
                "15000",
                "30000",
                "60000",
                "120000",
                "300000",
                "900000",
                "1800000",
                "3600000",
                "-1"
        });
    }
}