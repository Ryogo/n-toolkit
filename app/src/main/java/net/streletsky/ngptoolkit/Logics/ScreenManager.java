package net.streletsky.ngptoolkit.Logics;

import android.view.View;

import java.lang.reflect.InvocationTargetException;

public class ScreenManager {
    public static final int FULL_UPDATE = 536870917; // 536871010;
    public static final int A2_MODE = 268437764;

    public static void FullRefresh(View view) {
        Refresh(view, FULL_UPDATE);
    }

    public static void FastRefresh(View view) {
        Refresh(view, A2_MODE);
    }

    public static void Refresh(View view, int mode) {
        try {
            Class.forName("android.view.View").getDeclaredMethod("invalidate", new Class[]{Integer.TYPE}).invoke(view, Integer.valueOf(mode));
        } catch (IllegalAccessException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) { }
    }
}
