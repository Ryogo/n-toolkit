package net.streletsky.ngptoolkit.Logics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import net.streletsky.ngptoolkit.UI.ScreensaverView;
import net.streletsky.ngptoolkit.XSettings;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.util.Random;

public class ScreensaverManager {
    //static final String picsDir = "/sdcard/NOOK/Pics/sleep";
    static final String picsDir = "/system/usr/sleep/custom";
    static final String[] extensions = new String[] { "jpg", "png" };
    static final String defaultImagePath = "/system/usr/sleep/suspend.jpg";
    static String lastImagePath = null;

    static WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
            WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
            PixelFormat.TRANSPARENT
    );

    final Context context;
    final Handler handler;
    WindowManager windowManager;
    XSettings settings;
    Random randomizer;

    public ScreensaverManager(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
        windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        settings = new XSettings();
        randomizer = new Random();
    }

    public void show(final Runnable onShown) {
        final Bitmap image = GetNextImage();
        if (image == null) { return; }

        final XSettings settings = new XSettings();

        final ScreensaverView view = new ScreensaverView(context);
        view.setImageBitmap(image);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                view.postDelayed(onShown, 100);
            }
        });

        view.addOnDrawListener(drawCount -> {
            int removalDelay = settings.getRefreshScreenOnWake() ? 1500 : 0;

            if (drawCount == 1) {
                if (settings.getRefreshScreenOnWake()) {
                    view.postDelayed(() -> view.setImageResource(android.R.color.white), 1000);
                }
                if (settings.getIsFastRefreshEnabled()) {
                    view.setUpdateMode(ScreenManager.A2_MODE);
                }
            }
            if (drawCount == 2) {
                view.removeOnDrawListener();
                handler.postDelayed(() -> windowManager.removeView(view), removalDelay);
            }
        });

        handler.post(() -> windowManager.addView(view, layoutParams));
    }

    Bitmap GetNextImage() {
        File file = new File(picsDir);
        String[] files = new String[0];
        if (file.exists() && file.isDirectory()) {
            files = FileUtils.getFilesFromExtension(picsDir, extensions);
        }

        if (files.length == 0) {
            return getDefaultImage();
        }
        if (files.length == 1) {
            return BitmapFactory.decodeFile(files[0]);
        }

        int imageIndex = 0;
        if (lastImagePath != null) {
            int newIndex = ArrayUtils.lastIndexOf(files, lastImagePath);
            if (!settings.getRandomizeSleepScreenOrder()) {
                imageIndex = newIndex + 1 > files.length - 1 ? 0 : newIndex + 1;
            } else {
                imageIndex = newIndex;
                while (newIndex == imageIndex) {
                    imageIndex = randomizer.nextInt(files.length);
                }
            }
        }

        lastImagePath = files[imageIndex];

        return BitmapFactory.decodeFile(lastImagePath);
    }

    Bitmap getDefaultImage() {
        return FileUtils.fileExists(defaultImagePath) ? BitmapFactory.decodeFile(defaultImagePath) : null;
    }
}