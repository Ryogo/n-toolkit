package net.streletsky.ngptoolkit.Logics;

import android.content.Context;
import android.os.PowerManager;
import android.provider.Settings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import de.robv.android.xposed.XposedHelpers;

public class FrontlightManager {
    final Context context;

    public FrontlightManager(Context context) {
        this.context = context;
    }

    Object getPowerManagerService() {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        return XposedHelpers.getObjectField(powerManager, "mService");
    }

    public int getSystemBrightness() {
        int brightness = 0;

        try {
            brightness = android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) { }


        return brightness;
    }

    public int getActualBrightness() {
        int brightness = 0;
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(new File("/sys/class/backlight/mxc_msp430_fl.0/actual_brightness")));
            brightness = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
        } finally {
            try {
                if (reader != null){
                    reader.close();
                }
            } catch (IOException e) { }
        }

        return brightness;
    }


    public boolean isBrightnessOverridenByWindowManager() {
        return (int)XposedHelpers.getObjectField(getPowerManagerService(), "mScreenBrightnessOverrideFromWindowManager") > -1;
    }

    public boolean isFrontlightOn() {
        return getActualBrightness() > 0;
    }

    public void setFrontlightState(boolean on) {
        XposedHelpers.callMethod(getPowerManagerService(), "setTemporaryScreenBrightnessSettingOverride", on ? getSystemBrightness() : 0);
    }
}
