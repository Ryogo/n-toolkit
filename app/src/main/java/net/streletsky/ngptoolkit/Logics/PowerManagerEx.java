package net.streletsky.ngptoolkit.Logics;

import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;

import de.robv.android.xposed.XposedHelpers;

public class PowerManagerEx {
    final Object phoneWindowManager;

    public PowerManagerEx(final Object phoneWindowManager) {
        this.phoneWindowManager = phoneWindowManager;
    }

    PowerManager getPowerManager() {
        return (PowerManager) XposedHelpers.getObjectField(phoneWindowManager, "mPowerManager");
    }

    Handler getHandler() {
        return (Handler)XposedHelpers.getObjectField(phoneWindowManager, "mHandler");
    }

    public boolean isScreenOn() {
        return getPowerManager().isScreenOn();
    }

    public void goToSleep() {
        XposedHelpers.callMethod(getPowerManager(), "goToSleep", SystemClock.uptimeMillis());
    }

    public void wakeUp() {
        XposedHelpers.callMethod(getPowerManager(), "wakeUp", SystemClock.uptimeMillis());
    }

    public void showPowerDialog() {
        getHandler().post(() -> XposedHelpers.callMethod(phoneWindowManager, "showGlobalActionsDialog"));
    }
}
