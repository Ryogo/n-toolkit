# N+ Toolkit #

An exposed framework module intended to fix some of the flaws of the Nook Glowlight Plus / Nook Glowlight 3 stock firmware and some of the flaws issued by using a third-party launchers.

### Features ###

* full access to internal storage over MTP (USB)
* controlling frontlight by 'home' button long-press
* disable wakeup on usb plug/unplug
* disable low battery warning
* custom screensaver
* disable default launcher
* enable default statusbar
* statusbar customization
* fast refresh (A2) mode